﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Palachanski.Task1.Handlers
{
    public class JsonHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.WriteFile("~/contact.json");
        }

        public bool IsReusable { get; private set; }
    }
}