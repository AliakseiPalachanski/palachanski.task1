﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Palachanski.Task1.AuthModule;

namespace Palachanski.Task1.Handlers
{
    /// <summary>
    /// Summary description for LogoutHandler
    /// </summary>
    public class LogoutHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.User.Identity.IsAuthenticated)
                Authentication.Logout();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}