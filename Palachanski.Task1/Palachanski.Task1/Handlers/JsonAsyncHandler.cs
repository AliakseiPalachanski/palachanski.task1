﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace Palachanski.Task1.Handlers
{
    public class JsonAsyncHandler : IHttpAsyncHandler
    {
        private delegate void ProcessRequestDelegate(HttpContext ctx);

        public void ProcessRequest(HttpContext context)
        {
            Thread.Sleep(5000);
            context.Response.ContentType = "application/json";
            context.Response.WriteFile("~/contact.json");
        }

        public bool IsReusable { get { return false; } }
        public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, object extraData)
        {
            var prg = new ProcessRequestDelegate(ProcessRequest); 
            return prg.BeginInvoke(context, cb, extraData);
        }

        public void EndProcessRequest(IAsyncResult result)
        {

        }
    }

}