﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Palachanski.Task1.Handlers
{
    /// <summary>
    /// Summary description for HHtmlHandler
    /// </summary>
    public class HHtmlHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var fileName = context.Request["filename"]+".hhtml";

            context.Response.ContentType = "text/plain";
            context.Response.WriteFile(fileName);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}