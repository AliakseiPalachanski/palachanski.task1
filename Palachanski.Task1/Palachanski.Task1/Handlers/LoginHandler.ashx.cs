﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Palachanski.Task1.AuthModule;

namespace Palachanski.Task1.Handlers
{
    /// <summary>
    /// Summary description for LoginHandler
    /// </summary>
    public class LoginHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var response = context.Response;
            var login = context.Request.Params["login"];
            var password = context.Request.Params["password"];
            if (login == null || password == null) return;
            var user = Authentication.Login(login,password);
            if (user == null)
            {
                response.ContentType = "application/text";
                response.StatusCode = 401;
                response.Status = "401 Unauthorized";
                response.Write("Invalid login attempt");
            }
            else
            {
                response.StatusCode = 200;
                response.Status = "200 Success";
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}