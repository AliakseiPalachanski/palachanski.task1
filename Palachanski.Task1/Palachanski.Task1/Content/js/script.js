﻿var CustomNamespace = CustomNamespace || {};

CustomNamespace.$ = function (id) {
    return document.getElementById(id);
}

CustomNamespace.getXmlHttpRequest = function () {
    var xmlhttp;
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return xmlhttp;
}

CustomNamespace.login = function () {
    var xmlhttp = CustomNamespace.getXmlHttpRequest();
    var loginPar = CustomNamespace.$("username").value;
    var passwordPar = CustomNamespace.$("password").value;
    var params = "login=" + loginPar + "&password=" + passwordPar;
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                CustomNamespace.loginResponseProceeding();
            }
            if (xmlhttp.status == 401) {
                CustomNamespace.$("login_error_message").removeClass("invisible");
            }
        }
    }
    xmlhttp.open("POST", "login.aspx", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.setRequestHeader("Content-length", params.length);
    xmlhttp.setRequestHeader("Connection", "close");
    xmlhttp.send(params);
}

CustomNamespace.logout = function () {
    var xmlhttp = CustomNamespace.getXmlHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            CustomNamespace.logoutResponseProceeding();
        }
    }
    xmlhttp.open("GET", "logout.aspx", true);
    xmlhttp.send();
}

CustomNamespace.loadData = function () {
    var xmlhttp = CustomNamespace.getXmlHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            CustomNamespace.loadDataResponseProceeding(xmlhttp.responseText);
        }
    }
    xmlhttp.open("GET", "contact.aspx", true);
    xmlhttp.send();
}

CustomNamespace.loginResponseProceeding = function () {
    CustomNamespace.$("login_form").addClass("invisible");
    CustomNamespace.$("load_data").innerHTML = CustomNamespace.getHHtml("LoadButton");
    CustomNamespace.$("load_button").setAttribute("onclick", "CustomNamespace.loadData()");
    var button = CustomNamespace.$("login_button");
    button.setAttribute("onclick", "CustomNamespace.logout()");
    button.innerHTML = "Logout";
}

CustomNamespace.logoutResponseProceeding = function () {
    CustomNamespace.$("load_data").innerHTML = "";
    var button = CustomNamespace.$("login_button");
    button.setAttribute("onclick", "CustomNamespace.$('login_form').switchClass('invisible')");
    button.innerHTML = "Login";
}

CustomNamespace.loadDataResponseProceeding = function (data) {
    CustomNamespace.$("load_data").innerHTML = CustomNamespace.getHHtml("JsonTab");
    var xmlhttp = CustomNamespace.getXmlHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var json = xmlhttp.response;
            CustomNamespace.tab1Proceeding(json);
            CustomNamespace.tab2Proceeding(json);
        }
    }
    xmlhttp.open("GET", "contact.aspx", true);
    xmlhttp.send();
}

CustomNamespace.getHHtml = function (hhtmlName) {
    var data;
    var xmlhttp = CustomNamespace.getXmlHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            data = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET", ".hhtml?filename=" + hhtmlName, false);
    xmlhttp.send();
    return data;
}

CustomNamespace.tab1Proceeding = function(json) {
    CustomNamespace.$("raw_json").innerHTML += json;
}

CustomNamespace.tab2Proceeding = function (json) {
    json = JSON.parse(json);
    CustomNamespace.createTree(json, 0, CustomNamespace.$("json_tree"), "");
}


CustomNamespace.tab = function (tab) {
    CustomNamespace.$('tab1').style.display = 'none';
    CustomNamespace.$('tab2').style.display = 'none';
    CustomNamespace.$('li_tab1').setAttribute("class", "");
    CustomNamespace.$('li_tab2').setAttribute("class", "");
    CustomNamespace.$(tab).style.display = 'block';
    CustomNamespace.$('li_' + tab).setAttribute("class", "active");
}


//json tree
CustomNamespace.createTree = function (json, lvl, parent, className) {
    for (var o in json) {

        var node = document.createElement("div");
        node.innerHTML = o + ": ";
        node.style.paddingLeft = 15 * lvl + "px";
        node.className = className;
        parent.appendChild(node);

        var obj = json[o];
        if (obj instanceof Array) {
            node.innerHTML += obj + "<br>";
            node.addClass('file');
        } else if (typeof (obj) == "object") {
            CustomNamespace.createTree(obj, lvl + 1, node, 'invisible');
            node.addClass('folder');
            node.onclick = function (e) { CustomNamespace.showNodes(this); e.stopPropagation() };
        } else {
            node.innerHTML += obj + "<br>";
            node.addClass('file');
        }
    }
}

CustomNamespace.showNodes = function (item) {
    var childs = item.childNodes;
    for (var i = 1; i < childs.length; i++) {
        childs[i].switchClass('invisible');
    }
}

HTMLElement.prototype.switchClass = function (className) {
    if (this.className.indexOf(className) > -1)
        this.removeClass(className);
    else
        this.addClass(className);
}

HTMLElement.prototype.addClass = function (add) {
    if (this.className.indexOf(add) <= -1)
        this.className += " " + add;
}

HTMLElement.prototype.removeClass = function (remove) {
    var newClassName = "";
    var i;
    var classes = this.className.split(" ");
    for (i = 0; i < classes.length; i++) {
        if (classes[i] !== remove) {
            newClassName += classes[i] + " ";
        }
    }
    this.className = newClassName;
}