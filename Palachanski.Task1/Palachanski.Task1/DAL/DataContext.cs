﻿using System.Configuration;
using System.Data.Entity;

namespace Palachanski.Task1.DAL
{
    public class DataContext :DbContext
    {
        private static readonly string ConnectionString =
            ConfigurationManager.ConnectionStrings["DbConnectionString"].ConnectionString;
        public DataContext() : base(ConnectionString)
        {
        }

        public DbSet<User> Users { get; set; }
    }
}