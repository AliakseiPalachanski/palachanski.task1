﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Palachanski.Task1.DAL;

namespace Palachanski.Task1.AuthModule
{
    public static class UserRepository
    {
        public static List<User> GetAll()
        {
            try
            {
                using (var context = new DataContext())
                {
                    var users = context.Users.AsQueryable().ToList();
                    return users;
                }
            }
            catch
            {
                // this is test mock. just in case
                return new List<User> { new User { Login = "user", Password = "password" } };
            }
        }

        public static User Get(string login, string password)
        {
            try
            {
                using (var context = new DataContext())
                {
                    var user = context.Set<User>().FirstOrDefault(u => u.Login == login && u.Password == password);
                    return user;
                }
            }
            catch { return null; }
        }
    }
}