﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Palachanski.Task1.AuthModule
{
    public class CustomAuthModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.AuthenticateRequest += onApplicationAuthenticateRequest;
            context.EndRequest += onApplicationEndRequest;
        }

        private void onApplicationAuthenticateRequest(object sender, EventArgs e)
        {
            var app = (HttpApplication)sender;
            var context = app.Context;
            context.User = Authentication.CurrentUser;
        }

        private void onApplicationEndRequest(object sender, EventArgs e)
        {
            Authentication.CurrentUser = null;
        }

        public void Dispose() {}
    }
}