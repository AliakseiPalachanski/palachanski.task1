﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using Palachanski.Task1.DAL;

namespace Palachanski.Task1.AuthModule
{
    public class UserIdentity : IIdentity
    {
        public UserIdentity(string login)
        {
            User = UserRepository.GetAll().FirstOrDefault(u => u.Login == login);
        }

        public User User { get; set; }

        public string Name
        {
            get
            {
                return User != null ? User.Login : "anonym";
            }
        }

        public string AuthenticationType
        {
            get
            {
                return typeof(User).ToString();
            }
        }


        public bool IsAuthenticated
        {
            get
            {
                return User != null;
            }
        }
    }
}