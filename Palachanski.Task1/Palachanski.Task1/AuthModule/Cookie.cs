﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Palachanski.Task1.AuthModule
{
    public static class Cookie
    {
        private const string CookieName = "__auth_cookie";

        public static void AddUserCookie(string login, HttpContext context)
        {
            var ticket = new FormsAuthenticationTicket(
                    1,
                    login,
                    DateTime.Now,
                    DateTime.Now.Add(FormsAuthentication.Timeout),
                    true,
                    String.Empty,
                    FormsAuthentication.FormsCookiePath);
            var secretTicket = FormsAuthentication.Encrypt(ticket);
            var authCookie = new HttpCookie(CookieName, secretTicket)
            {
                Expires = DateTime.Now.Add(FormsAuthentication.Timeout)
            };
            context.Response.SetCookie(authCookie);
        }

        public static FormsAuthenticationTicket ReadUserCookie(HttpContext context)
        {
            FormsAuthenticationTicket ticket = null;
            var cookie = context.Request.Cookies[CookieName];
            if (cookie != null && !String.IsNullOrEmpty(cookie.Value))
            {
                ticket = FormsAuthentication.Decrypt(cookie.Value);
            }
            return ticket;
        }

        public static void DeleteUserCookie(HttpContext context)
        {
            context.Request.Cookies.Remove(CookieName);
        }
    }
}