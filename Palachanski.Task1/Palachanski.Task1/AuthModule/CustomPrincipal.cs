﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace Palachanski.Task1.AuthModule
{
    public class CustomPrincipal : IPrincipal
    {
        public CustomPrincipal(string login)
        {
            userIdentity = new UserIdentity(login);
        }

        private UserIdentity userIdentity { get; set; }

        public bool IsInRole(string role)
        {
            return userIdentity.User != null;
        }

        public IIdentity Identity
        {
            get
            {
                return userIdentity;
            }
        }
    }
}