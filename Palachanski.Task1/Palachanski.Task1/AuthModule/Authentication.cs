﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using Palachanski.Task1.DAL;

namespace Palachanski.Task1.AuthModule
{
    public static class Authentication
    {
        private static IPrincipal _currentUser;

        public static IPrincipal CurrentUser
        {
            get
            {
                if (_currentUser != null) return _currentUser;
                var ticket = Cookie.ReadUserCookie(HttpContext.Current);
                if (ticket != null && ticket.Name != null)
                    _currentUser = new CustomPrincipal(ticket.Name);
                else
                    _currentUser = null;
                return _currentUser;
            }
            set { _currentUser = value; }
        }

        public static User Login(string login, string password)
        {
            var user = UserRepository.GetAll().FirstOrDefault(u => u.Login == login && u.Password == password);
            if (user!=null)
            {
                Cookie.AddUserCookie(login,HttpContext.Current);
            }
            return user;
        }

        public static void Logout()
        {
            Cookie.DeleteUserCookie(HttpContext.Current);
            CurrentUser = null;
        }
    }
}