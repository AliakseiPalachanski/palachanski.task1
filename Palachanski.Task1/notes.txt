Custom authentication implemented via custom handlers and modules.
The only test user in database is user with credentials: "user" "password".
When authenticated press "LoadData" to load json object. Json is presented in the raw and in the form of tree.

I have used codefirst approach and I'm just not sure that database will correctly recreate, so I have added test mock repository in exception handling just in case.